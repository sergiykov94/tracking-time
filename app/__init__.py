import logging
from logging.handlers import SMTPHandler, RotatingFileHandler
import os
from flask import Flask, request, current_app
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import LoginManager
from flask_bootstrap import Bootstrap
from tracking_time.config import Config
from flask_moment import Moment

db = SQLAlchemy()
migrate = Migrate()
login = LoginManager()
login.login_view = 'auth.login'
bootstrap = Bootstrap()
moment = Moment()

def create_app(config_class=Config):
    app = Flask(__name__)
    app.config.from_object(config_class)

    db.init_app(app)
    migrate.init_app(app, db)
    login.init_app(app)
    bootstrap.init_app(app)
    moment.init_app(app)

    from tracking_time.app.errors import bp as errors_bp
    app.register_blueprint(errors_bp)

    from tracking_time.app.auth import bp as auth_bp
    app.register_blueprint(auth_bp, url_prefix='/auth')

    from tracking_time.app.api import bp as api_bp
    app.register_blueprint(api_bp, url_prefix='/api')

    from tracking_time.app.main import bp as main_bp
    app.register_blueprint(main_bp)


    return app
