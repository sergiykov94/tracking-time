from flask import Blueprint

bp = Blueprint('errors', __name__)

from tracking_time.app.errors import handlers