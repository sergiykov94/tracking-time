import json
from flask_login import current_user
from flask import jsonify, request, url_for, g, abort, redirect, render_template
from datetime import datetime
from tracking_time.app import db
from tracking_time.app.api import bp
from tracking_time.app.api.errors import bad_request
from tracking_time.app.models import User, RegisterTime

# testing
@bp.route('/time-tracking/start', methods=['GET', 'POST'])
def start_time():
    if not current_user.is_authenticated:
        return jsonify({"status": "fail"})
    user_id = current_user.id
    if RegisterTime.query.filter_by(
        user_id = user_id,
        end_time=None
    ).first():
        return bad_request('you already start time')
    timer = RegisterTime(
        user_id = user_id
    )
    db.session.add(timer)
    db.session.commit()
    time = timer.start_time.strftime("%m/%d/%Y %I:%M %p")
    return jsonify({ 'id': timer.id,'time': time}), 201

# testing
@bp.route('/time-tracking/stop', methods=['POST'])
def stop_time():
    if request.method == 'POST':
        data = request.json
        minutes_total = 0
        comment_from_request = data.get('comment')
        task_from_request = data.get('tasks')
        timer_not_started = RegisterTime.query.filter_by(
            start_time=None,
            user_id = current_user.id
        ).first()
        if timer_not_started:
            return bad_request('You must start timer first')
        if comment_from_request == '':
            return jsonify({'error': 'Please write comment'}), 400
        if task_from_request == '':
            return jsonify({'error': 'Please write task'}), 400
        timer = RegisterTime.query.filter_by(
            end_time = None,
            user_id = current_user.id
        ).first()
        if not timer:
            return bad_request(f'timer not found for {current_user.id}')
        timer.end_time = datetime.today()
        timer.comment = comment_from_request
        timer.tasks = task_from_request
        check_time = timer.end_time - timer.start_time
        minutes_total = minutes_total + (check_time.seconds // 60) / 60
        db.session.add(timer)
        db.session.commit()
        time = timer.end_time.strftime("%m/%d/%Y %I:%M %p")
        comments = timer.comment
        task = timer.tasks
        time_reprentation = "{}:{}".format(check_time.seconds / 60 // 60, check_time.seconds / 60)
        return jsonify({'id': timer.id, 'time': time, 'comments': comments, 'task': task, 'time_reprentation': time_reprentation, }), 201

# testing
@bp.route('/edit/<id_>', methods=['POST', 'GET'])
def edit_time(id_):
    record = db.session.query(RegisterTime).filter(
        RegisterTime.id == id_ 
    ).first()
    if request.method == 'GET':
        return jsonify(
            {
                'comment' : record.comment,
                'task': record.tasks,
            }
        ), 201
    if request.method == 'POST':
        data = request.json
        comment_edit = data.get('edit_comment')
        task_edit = data.get('edit_task')
        record.comment = comment_edit
        record.tasks = task_edit
        db.session.add(record)
        db.session.commit()
        return jsonify({'record.comment': record.comment, 'record.tasks': record.tasks}), 201
