from flask import jsonify, g
from tracking_time.app import db
from tracking_time.app.api import bp
from tracking_time.app.api.auth import basic_auth

@bp.route('/tokens', methods=['POST'])
@basic_auth.login_required
def get_token():
    token = g.current_user.get_token()
    db.session.commit()
    return jsonify({'token': token})