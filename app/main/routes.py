from datetime import datetime, timedelta, time
import os
from flask import Flask, render_template, flash, redirect, url_for, request, \
    jsonify, current_app, send_from_directory
from flask_login import current_user, login_user, login_required, logout_user
from tracking_time.app import db
from tracking_time.app.models import User, RegisterTime, Photos
from tracking_time.app.main import bp
from tracking_time.app.api import time_api
from tracking_time.app.api.errors import bad_request
from werkzeug.utils import secure_filename
from tracking_time.app.auth.forms import LoginForm, RegistrationForm

UPLOAD_FOLDER = '/Users/sergiy/study/tracking_time/photo'
ALLOWED_EXTENSIONS = {'jpg', 'jpeg', 'gif'}

app = Flask(__name__, static_url_path='')
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@bp.route('/uploads/<filename>')
def uploaded_file(filename):
    return send_from_directory(app.config['UPLOAD_FOLDER'],
                               filename)

@bp.route('/static/<filename>')
def send_js(path):
    return send_from_directory('/Users/sergiy/study/tracking_time/static', filename)

@bp.route('/api/upload', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        data = request.json
        if 'file' not in request.files:
            return jsonify({"error": "No file"}), 400
        file = request.files['file']
        newfile = Photos(filename = file.filename, user_id = current_user.id)
        if file.filename == '':
            return jsonify({"error": "No selected file"}), 400
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            db.session.add(newfile)
            db.session.commit()
            return jsonify({'file': filename}), 201
    return jsonify({"status": "fail"})


@bp.route('/user', methods=['GET', 'POST'])
@login_required
def show_profile():
    show_photo = db.session.query(Photos).filter(
        Photos.id == current_user.id
    ).first()
    return render_template('user.html', show_photo = show_photo)


@bp.route('/index', methods=['GET', 'POST'])
@login_required
def index():
    user_times = db.session.query(RegisterTime).filter(
        RegisterTime.user_id == current_user.id,
        RegisterTime.deleted == False
    ).all()
    minutes_total = 0
    user_time_set = []
    check_time = None
    for time in user_times:
        if time.end_time:
            payload = {
                'id': time.id,
                'start_time' : time.start_time.strftime("%m/%d/%Y %I:%M %p"),
                'end_time' : time.end_time.strftime("%m/%d/%Y %I:%M %p"),
            }
            check_time = time.end_time - time.start_time
            minutes_total = minutes_total + (check_time.seconds // 60) / 60
            payload.update({'check_time' : check_time })
        else:
            payload = {
                'id': time.id,
                'start_time' : time.start_time.strftime("%m/%d/%Y %I:%M %p")
            }
        if time.comment and time.tasks is not None:
            payload.update({ 
                'comment' : time.comment,
                'tasks' : time.tasks 
            })
        user_time_set.append(payload)
    return render_template('index.html', check_time = check_time, user_time_set = user_time_set, minutes_total=round(minutes_total, 2)) 


@bp.route('/calendar', methods=['GET'])
@login_required
def check_calendar():
    start_time_cal = request.args.get('start_time_from_request')
    end_time_cal = request.args.get('end_time_from_request')
    time_list_set = []
    sum_hours = None
    sum_hours_all = 0
    if start_time_cal and end_time_cal :
        start_time = datetime.strptime(start_time_cal, "%Y-%m-%d")
        end_time = datetime.strptime(end_time_cal, "%Y-%m-%d")
        interval = db.session.query(RegisterTime).filter(
            RegisterTime.start_time >= start_time,
            RegisterTime.end_time <= end_time,
            RegisterTime.user_id == current_user.id,
            RegisterTime.deleted == False
        ).all()
        for date in interval:
            payload = {
            'id': date.id,
            'start_time' : date.start_time,
            'end_time' : date.end_time
            }
            sum_hours = date.end_time - date.start_time
            sum_hours_all = sum_hours_all + (sum_hours.seconds // 60) / 60
            payload.update({'sum_hours_all' : sum_hours_all })
            time_list_set.append(payload)
        return render_template('calendar.html', interval=interval, start_time_cal=start_time_cal, 
                end_time_cal=end_time_cal, sum_hours_all = sum_hours_all)
    return render_template('calendar.html', start_time_cal=start_time_cal, end_time_cal=end_time_cal)


@bp.route('/delete/<id_>/yes', methods = ['GET'])
@login_required
def delete_view(id_):
    delete_all = RegisterTime.query.get_or_404(id_)
    if delete_all.deleted:
        flash('alredy deleted')
    delete_all.deleted = True
    db.session.commit()
    return redirect(url_for('main.check_calendar'))

# testing
@bp.route('/api/remove/<record_id>', methods = ['DELETE'])
def delete_time_view(record_id):
    delete_all = RegisterTime.query.get_or_404(record_id)
    delete_all.deleted = True
    db.session.commit()
    return jsonify({'deleted': True}), 201

# testing
@bp.route('/api/return/delete/<record_id>', methods = ['POST'])
def return_delete_record(record_id):
    delete_all = RegisterTime.query.get_or_404(record_id)
    delete_all.deleted = False
    db.session.commit()
    return jsonify({'returned': False}), 201

# testing
@bp.route('/api/edit/username', methods = ['GET', 'POST'])
@login_required
def edit_information():
    change_email = current_user.email
    change_username = current_user.username
    if request.method == 'GET':
        return jsonify(
            {
            'change_email' : change_email,
            'change_username' : change_username
            }
        ), 201
    if request.method == 'POST':
        data = request.json
        edit_email = data.get('edit_user_email')
        edit_name = data.get('edit_username')
        current_user.email = edit_email
        current_user.username = edit_name
    if edit_email.strip() == '':
        return jsonify({'error': 'Please write email'}), 400
    if edit_name.strip() == '':
        return jsonify({'error': 'Please write username'}), 400
    db.session.add(current_user)
    db.session.commit()
    return jsonify(
            {
            'username': current_user.username,
            'email': current_user.email
            }
        ), 201

# testing
@bp.route('/api/create_new_password', methods=['POST'])
def change_password_function():
    if request.method == 'POST':
        data = request.json
        current_password = data.get('change_password')
        new_password = data.get('change_password_first')
        new_password_two = data.get('change_password_second')
    if new_password != new_password_two:
        flash('error please doing same password')
    if current_user.check_password(current_password):
        current_user.set_password(new_password)
        db.session.add(current_user)
        db.session.commit()
        flash('Congratulations, you are change you password change!')
        return jsonify({'new_password': new_password, 'new_password_two': new_password_two})
    return jsonify({'error': 'old password did not match'})

@bp.route('/admin', methods=['GET', 'POST'])
@login_required
def admin_room():
    admin_user = db.session.query(User).filter(
        User.id == current_user.id
    ).first()
    if not admin_user.administrarion:
        return redirect(url_for('main.index'))
    start_time_cal = request.args.get('start_time_from_request')
    end_time_cal = request.args.get('end_time_from_request')
    search_user = request.args.get('search_user_from_request')
    is_deleted = request.args.get('request_checkbox')
    user = None
    interval = db.session.query(RegisterTime)
    if start_time_cal and end_time_cal:
        start_time = datetime.strptime(start_time_cal, "%Y-%m-%d")
        end_time = datetime.strptime(end_time_cal, "%Y-%m-%d")
        interval = interval.filter(
            RegisterTime.start_time >= start_time,
            RegisterTime.end_time <= end_time,
            RegisterTime.deleted == False,
        )
    if is_deleted:
        interval = interval.filter(RegisterTime.deleted == True)
    if search_user:
        user = db.session.query(User).filter(
            User.username == search_user
        ).first()
        if not user:
            flash('error user name is not found')
            return redirect(url_for('main.admin_room'))
    time_list_set = []
    if user:
        interval = interval.filter(RegisterTime.user_id == user.id)
    for date in interval.all():
        payload = {
        'id' : date.id,
        'user_id' : date.user.id,
        'username': date.user.username,
        'start_time' : date.start_time.strftime("%m/%d/%Y %I:%M %p"),
        'end_time' : date.end_time.strftime("%m/%d/%Y %I:%M %p"),
        'deleted' : date.deleted
        }
        if date.comment and date.tasks is not None:
            payload.update({
                'comment' : date.comment,
                'tasks' : date.tasks
                })
        time_list_set.append(payload)
    return render_template('admin_panel.html', interval = interval, time_list_set = time_list_set)

# testing
@bp.route('/api/edit_all/<id_>', methods = ['POST', 'GET'])
@login_required
def edit_time_admin(id_):
    edit_admin_db = db.session.query(RegisterTime).filter(
        RegisterTime.id == id_
    ).first()
    if request.method == 'GET':
        return jsonify(
            {
                'comment' : edit_admin_db.comment,
                'task': edit_admin_db.tasks,
                'start_time': edit_admin_db.start_time.strftime("%m/%d/%Y %I:%M %p"),
                'end_time': edit_admin_db.end_time.strftime("%m/%d/%Y %I:%M %p"),
            }
        ), 201
    if request.method == 'POST':
        data = request.json
        print(data)
        comment_edit_admin = data.get('edit_comment_admin')
        task_edit_admin = data.get('edit_task_admin')
        start_time_edit = data.get('edit_start_time_admin')
        end_time_edit = data.get('edit_end_time_admin')
        if start_time_edit >= end_time_edit:
            return jsonify({"error": "start time >= end time"}), 400
        edit_admin_db.comment = comment_edit_admin
        edit_admin_db.tasks = task_edit_admin
        edit_admin_db.start_time = datetime.strptime(start_time_edit, "%m/%d/%Y %I:%M %p")
        edit_admin_db.end_time = datetime.strptime(end_time_edit, "%m/%d/%Y %I:%M %p")
        db.session.commit()
        return jsonify(
            {
            'comment' : edit_admin_db.comment,
            'tasks': edit_admin_db.tasks,
            'start_time': edit_admin_db.start_time.strftime("%m/%d/%Y %I:%M %p"),
            'end_time': edit_admin_db.end_time.strftime("%m/%d/%Y %I:%M %p")
            }
        ),201

