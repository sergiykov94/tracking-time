from flask import Blueprint

bp = Blueprint('main', __name__)

from tracking_time.app.main import routes