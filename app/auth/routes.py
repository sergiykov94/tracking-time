from flask import render_template, redirect, url_for, flash, request
from werkzeug.urls import url_parse
from flask_login import current_user, login_user, login_required, logout_user
from tracking_time.app import db
from tracking_time.app.auth import bp
from datetime import datetime, timedelta, time
from tracking_time.app.auth.forms import LoginForm, RegistrationForm, ResetPasswordRequestForm, Change_password
from tracking_time.app.models import User, RegisterTime

@bp.route('/login', methods=['GET', 'POST'])
def login():
    administration_user = db.session.query(User).filter(
        User.administrarion == True
    ).all()
    if current_user.is_authenticated:
        return redirect(url_for('main.index'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(
            username=form.username.data
        ).first()
        if user is None or not user.check_password(form.password.data):
            flash('Invalid username or password')
            return redirect(url_for('auth.login'))
        login_user(user, remember=form.remember_me.data)
        next_page = request.args.get('next')
        if user.administrarion:
            return redirect(url_for('main.admin_room'))
        if not next_page or url_parse(next_page).netloc != '':
            next_page = url_for('main.index')
        return redirect(next_page)
    return render_template('auth/login.html', title='Sign In', form=form, administration_user = administration_user)

@bp.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('main.index'))

@bp.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('main.index'))
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(username=form.username.data, email=form.email.data, question=form.question_name.data, answer=form.answer_name.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('Congratulations, you are now a registered user!')
        return redirect(url_for('auth.register'))
    return render_template('auth/register.html', title='Register', form=form)

@bp.route('/reset_password_request', methods=['GET', 'POST'])
def reset_password_request():
    if current_user.is_authenticated:
        return redirect(url_for('main.index'))
    form = ResetPasswordRequestForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        print('current_user', user)
        if user:
            return redirect(url_for('auth.change_pw', user_id = user.id))
    return render_template('forgot_pw.html',
                            title='Reset Password', form=form)

@bp.route('/check_user_question/<user_id>', methods=['GET', 'POST'])
def change_pw(user_id):
    user = User.query.filter_by(id = user_id).one()
    if request.method == 'GET':
        return render_template('check_secret_question.html', user_id=user_id, question = user.question)
    if request.method == 'POST':
        user_id = request.form.get('user_id')
        user_answer = request.form.get('user_answer_form')
        user = User.query.filter_by(id = user_id).one()
        if user.answer == user_answer:
            return redirect(url_for('auth.forgot_passwords', user_id = user_id))
        if user.answer != user_answer:
            flash('Error please write another answer')
            return redirect(url_for('auth.reset_password_request'))
        return redirect(url_for('auth.change_pw'))

@bp.route('/change_password/<user_id>', methods=['POST', 'GET'])
def forgot_passwords(user_id):
    user = User.query.filter_by(id=user_id).one()
    if request.method == 'GET':
        return render_template('password_change.html')
    if request.method == 'POST':
        new_password = request.form.get('change_password_first')
        new_password_two = request.form.get('change_password_second')
        if new_password != new_password_two:
            flash('error please doing same password')
            return redirect(url_for('main.change_pw'))
        user.set_password(new_password)
        db.session.add(user)
        db.session.commit()
        flash('Congratulations, you change you password!')
        return redirect(url_for('main.index'))