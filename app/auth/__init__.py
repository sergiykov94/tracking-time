from flask import Blueprint

bp = Blueprint('auth', __name__)

from tracking_time.app.auth import routes