FROM python:3.6-alpine

RUN adduser -D tracking

WORKDIR /Users/sergiy/study/tracking_time

COPY requirements.txt requirements.txt
RUN python -m venv venv
RUN venv/bin/pip install -r requirements.txt
RUN venv/bin/pip install gunicorn

COPY app app
COPY migrations migrations
COPY tracking.py config.py boot.sh ./
RUN chmod +x boot.sh

ENV FLASK_APP tracking.py

RUN chown -R tracking:tracking ./
USER tracking

EXPOSE 5000