import os
import tempfile

import base64
import pytest
from flask import _request_ctx_stack
from functools import partial
from flask_login import current_user, login_user, login_required
from tracking_time.app.models import User, RegisterTime
from tracking_time.app import create_app, db as _db
from datetime import datetime
'''“@pytest.fixture” — декоратор, указывающий, что функция ниже является фикстурой'''


def force_authenticate_handler(app, user):
    """
        Method shouuld force authenticate
    """
    setattr(_request_ctx_stack.top, 'user', user)


@pytest.fixture
def force_authenticate(app):
    """
        Method should force authenticate user
    """
    return partial(force_authenticate_handler, app)


def create_user_handler(db, username, email):
        user = User(username=username, email=email, question='hello', answer='im fine')
        user.set_password('1234')
        db.session.add(user)
        db.session.commit()
        return user

def create_start_time_handler(db, start_time, user_id):
        timer = RegisterTime(start_time=start_time, user_id=user_id)
        db.session.add(timer)
        db.session.commit()
        return timer

def headers_handler(user_id):
    basic_fmt = 'Basic {0}'
    decoded = bytes.decode(base64.b64encode(str.encode(str(user_id))))
    return [('Authorization', basic_fmt.format(decoded))]

def create_comment_tasks_handler(db, user_id, comment, tasks):
        timer = RegisterTime(user_id=user_id, comment=comment, tasks=tasks)
        db.session.add(timer)
        db.session.commit()
        return timer

def create_comment_tasks_start_stop_time_handler(db, user_id, comment, tasks, start_time, end_time):
        timer = RegisterTime(user_id=user_id, comment=comment, tasks=tasks, start_time=start_time, end_time=end_time)
        db.session.add(timer)
        db.session.commit()
        return timer


@pytest.fixture
def headers():
    return partial(headers_handler)

@pytest.fixture()
def create_user(db):
    return partial(create_user_handler, db)

@pytest.fixture()
def create_start_time(db):
    return partial(create_start_time_handler, db)

@pytest.fixture()
def create_comment_tasks(db):
    return partial(create_comment_tasks_handler, db)

@pytest.fixture()
def create_comment_tasks_start_stop_time(db):
    return partial(create_comment_tasks_start_stop_time_handler, db)

@pytest.fixture(scope='session')
def app():
    app = create_app()
    return app


@pytest.fixture(scope='session')
def db(app):
    _db.app = app
    # TODO: investigate move creating extension to alembic config
    db_fd, app.config['DATABASE'] = tempfile.mkstemp()
    app.config['TESTING'] = True
    with app.app_context():
        _db.create_all()

    yield _db

    _db.session.close()
    _db.drop_all()
    os.close(db_fd)
    os.unlink(app.config['DATABASE'])



@pytest.fixture
def api_client(app):
    with app.test_client() as client:
        yield client

