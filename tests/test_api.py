from freezegun import freeze_time
from datetime import datetime, timedelta
from unittest.mock import patch
from tracking_time.app.models import User, RegisterTime
from flask_login import login_user

@patch('flask_login.utils._request_ctx_stack')
def test_edit_username_check(request_ctx_stack, api_client, 
    create_user, force_authenticate):
    user = create_user('Andy', 'Kovv')
    request_ctx_stack.top.current_identity = user
    request_ctx_stack.top.user = user
    response = api_client.get('/api/edit/username')
    data = response.get_json()
    assert data['change_username'] == 'Andy'
    assert data['change_email'] == 'Kovv'

@patch('flask_login.utils._request_ctx_stack')
def test_change_username_and_email(request_ctx_stack, api_client, 
    create_user, force_authenticate):
    user = create_user('petja', 'petja@gmail.com')
    request_ctx_stack.top.current_identity = user
    request_ctx_stack.top.user = user
    response = api_client.post('/api/edit/username', json= {
        'edit_username' : 'sasha',
        'edit_user_email': 'sasha@gmail.com'
        })
    assert response.status_code == 201
    data = response.get_json()
    assert data['username'] == 'sasha'
    assert data['email'] == 'sasha@gmail.com'

@patch('flask_login.utils._request_ctx_stack')
def test_change_password(request_ctx_stack, api_client,
    create_user, force_authenticate):
    user = create_user('vanja', 'vanja@gmail.com')
    request_ctx_stack.top.current_identity = user
    request_ctx_stack.top.user = user
    response = api_client.post('/api/create_new_password', json={
        'change_password' : '1234',
        'change_password_first' : '4321',
        'change_password_second' : '4321',
        })
    data = response.get_json()
    assert data['new_password'] == '4321'
    assert data['new_password_two'] == '4321'

@patch('flask_login.utils._request_ctx_stack')
def test_start_time(request_ctx_stack, api_client, create_user,
 create_start_time, force_authenticate):
    user = create_user('sera', 'sera@gmail.com')
    time = datetime.now().strftime("%m/%d/%Y %I:%M %p")
    request_ctx_stack.top.current_identity = user
    request_ctx_stack.top.user = user
    response = api_client.post('api/time-tracking/start')
    assert response.status_code == 201
    data = response.get_json()
    assert data['time'] == time

@patch('flask_login.utils._request_ctx_stack')
def test_stop_time(request_ctx_stack, api_client, create_user, 
    create_start_time, force_authenticate):
    user = create_user('serafim', 'serafim@gmail.com')
    timer = create_start_time(datetime.today(), user.id)
    time = datetime.today().strftime("%m/%d/%Y %I:%M %p")
    request_ctx_stack.top.current_identity = user
    request_ctx_stack.top.user = user
    response = api_client.post('api/time-tracking/stop', json={
        'comment': 'done',
        'tasks': 'add db',
        })
    assert response.status_code == 201
    data = response.get_json()
    assert data['time'] == time
    assert data['comments'] == 'done'
    assert data['task'] == 'add db'

@patch('flask_login.utils._request_ctx_stack')
def test_should_ceck_start_and_end_time_logic(
    request_ctx_stack, api_client, app,
    create_user, headers
): 
    user_name = 'some_user'
    user_email = 'some@email.com'
    user = create_user(user_name, user_email)
    request_ctx_stack.top.current_identity = user
    request_ctx_stack.top.user = user

    with freeze_time(datetime.now()):
        response = api_client.post('api/time-tracking/start', headers=headers(user.id))
        assert response.status_code == 201

    hour = datetime.now() + timedelta(minutes=60)
    with freeze_time(hour):
        response = api_client.post('api/time-tracking/stop', headers=headers(user.id), json={
            'comment': 'done',
            'tasks': 'add db',
            })
        assert response.status_code == 201

    records = RegisterTime.query.filter(RegisterTime.user_id == user.id).count()
    assert records == 1

    record = RegisterTime.query.filter(RegisterTime.user_id == user.id).first()
    assert record.end_time > record.start_time

@patch('flask_login.utils._request_ctx_stack')
def test_comment_tasks(request_ctx_stack, api_client, 
    create_user, create_start_time,
    create_comment_tasks, force_authenticate):
    user = create_user('pasha', 'Same')
    request_ctx_stack.top.current_identity = user
    request_ctx_stack.top.user = user
    timer = create_start_time(datetime.today(), user.id)
    timer = create_comment_tasks(user.id, 'im done', 'doing')
    response = api_client.get(f'api/edit/{timer.id}')
    assert response.status_code == 201
    data = response.get_json()
    assert data['comment'] == 'im done'
    assert data['task'] == 'doing'

@patch('flask_login.utils._request_ctx_stack')
def test_edit_comment_tasks(request_ctx_stack, api_client, 
    create_user, create_start_time,
    force_authenticate):
    user = create_user('dima', 'haer')
    request_ctx_stack.top.current_identity = user
    request_ctx_stack.top.user = user
    timer = create_start_time(datetime.today(), user.id)
    response = api_client.post(f'api/edit/{timer.id}', json={
        'edit_comment': 'im done2',
        'edit_task': 'doing2',
        })
    assert response.status_code == 201
    data = response.get_json()
    assert data['record.comment'] == 'im done2'
    assert data['record.tasks'] == 'doing2'

@patch('flask_login.utils._request_ctx_stack')
def test_remove_timetrack(request_ctx_stack, api_client, 
    create_user, create_start_time,
    force_authenticate):
    user = create_user('pash2a', 'Sa2me')
    request_ctx_stack.top.current_identity = user
    request_ctx_stack.top.user = user
    timer = create_start_time(datetime.today(), user.id)
    response = api_client.delete(f'/api/remove/{timer.id}')
    assert response.status_code == 201
    data = response.get_json()
    assert data['deleted'] is True

@patch('flask_login.utils._request_ctx_stack')
def test_return_timetrack(request_ctx_stack, api_client, 
    create_user, create_start_time,
    force_authenticate):
    user = create_user('sergiy1','kov1')
    request_ctx_stack.top.current_identity = user
    request_ctx_stack.top.user = user
    timer = create_start_time(datetime.today(), user.id)
    response = api_client.post(f'/api/return/delete/{timer.id}')
    assert response.status_code == 201
    data = response.get_json()
    assert data['returned'] is False

@patch('flask_login.utils._request_ctx_stack')
def test_comment_tasks_admin_edit_all(request_ctx_stack, api_client, 
    create_user, create_start_time,
    create_comment_tasks_start_stop_time, force_authenticate):
    user = create_user('pasha1', 'Same1')
    request_ctx_stack.top.current_identity = user
    request_ctx_stack.top.user = user
    time_now = datetime.now()
    time = datetime.now() + timedelta(minutes=60)
    timer = create_comment_tasks_start_stop_time(user.id, 'im done', 'doing', time_now, time)

    with freeze_time():
        response = api_client.get(f'/api/edit_all/{timer.id}')
        assert response.status_code == 201
        data = response.get_json()
        assert data['comment'] == 'im done'
        assert data['task'] == 'doing'
        assert data['start_time'] == time_now.strftime("%m/%d/%Y %I:%M %p")
        assert data['end_time'] == time.strftime("%m/%d/%Y %I:%M %p")

@patch('flask_login.utils._request_ctx_stack')
def test_comment_tasks_admin(request_ctx_stack, api_client, 
    create_user, create_start_time,
    create_comment_tasks_start_stop_time, force_authenticate):
    user = create_user('pasha12', 'Same12')
    request_ctx_stack.top.current_identity = user
    request_ctx_stack.top.user = user
    time_now = datetime.now()
    time = datetime.now() + timedelta(minutes=60)
    timer = create_comment_tasks_start_stop_time(user.id, 'im done', 'doing', time_now, time)

    with freeze_time():
        response = api_client.post(f'/api/edit_all/{timer.id}', json={
            'edit_comment_admin' : '123',
            'edit_task_admin' : '321',
            'edit_start_time_admin': time_now.strftime("%m/%d/%Y %I:%M %p"),
            'edit_end_time_admin' : time.strftime("%m/%d/%Y %I:%M %p"),
            })
        assert response.status_code == 201